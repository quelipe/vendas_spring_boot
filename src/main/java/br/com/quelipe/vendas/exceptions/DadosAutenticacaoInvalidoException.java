package br.com.quelipe.vendas.exceptions;

public class DadosAutenticacaoInvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DadosAutenticacaoInvalidoException() {
		super("Dados de autenticação inválidos.");
	}

	
}
