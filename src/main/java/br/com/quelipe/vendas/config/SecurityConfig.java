package br.com.quelipe.vendas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.quelipe.vendas.jwt.JwtAuthFilter;
import br.com.quelipe.vendas.jwt.JwtService;
import br.com.quelipe.vendas.services.UsuarioService;


@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Autowired
	@Lazy
	private UsuarioService usuarioService;
	
	@Autowired
	private JwtService jwtservice;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public OncePerRequestFilter jwtFilter() {
		return new JwtAuthFilter(jwtservice, usuarioService);
	}
	
	@Bean
	public SecurityFilterChain configureChain(HttpSecurity http) throws Exception {
		http
			.cors().and().csrf().disable() 
			.securityMatcher("/api/**")
        	.authorizeHttpRequests(requests -> requests
	            //.requestMatchers("/api/v1/clientes/**").hasAnyRole("ADMIN")
	            .requestMatchers("/api/v1/usuarios/**").permitAll()
				.anyRequest().authenticated()
			//).httpBasic();
			)
        	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        	.addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}
 
}
