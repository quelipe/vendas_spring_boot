package br.com.quelipe.vendas.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.quelipe.vendas.domain.entity.Produto;
import br.com.quelipe.vendas.services.ProdutoService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/produtos")
public class ProdutoController {
	
	@Autowired
	private ProdutoService service;

	@GetMapping
	public List<Produto> findAll() {
		return service.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public Produto findById(@PathVariable Integer id) {
		return service.findById(id);
	}
	
	@GetMapping("/filtro")
	public List<Produto> perquisar(Produto filtro) {
		return service.pesquisar(filtro);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Produto incluir(@RequestBody @Valid Produto produto) {
		return service.incluir(produto);		
	}
	
	@PutMapping("{id}")
	public Produto atualizar(@PathVariable Integer id, @RequestBody @Valid Produto produto) {
		return service.atualizar(id, produto);
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		service.excluir(id);
	}
	
	
}
