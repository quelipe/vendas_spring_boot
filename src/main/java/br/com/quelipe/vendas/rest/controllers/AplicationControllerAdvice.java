package br.com.quelipe.vendas.rest.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.quelipe.vendas.exceptions.RecursoNaoEncontradoException;
import br.com.quelipe.vendas.exceptions.RegraNegocioException;
import br.com.quelipe.vendas.rest.ApiErrorsMessage;
import br.com.quelipe.vendas.rest.ApiResponseErro;
import jakarta.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class AplicationControllerAdvice {

	@ExceptionHandler(RegraNegocioException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErrorsMessage handleRegraNegocioException(RegraNegocioException ex) {
		String mensagemErro = ex.getMessage();
		return new ApiErrorsMessage(mensagemErro);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErrorsMessage handleMothodNotValidException(MethodArgumentNotValidException ex) {
		List<String> erros = ex.getBindingResult().getAllErrors()
				.stream()
				.map(erro -> erro.getDefaultMessage())
				.toList();
		return new ApiErrorsMessage(erros);
	}
	
	@ExceptionHandler(RecursoNaoEncontradoException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ApiResponseErro handleResourceNotFoundException(RecursoNaoEncontradoException e, HttpServletRequest request) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		return new ApiResponseErro(new Date(), status.value(), status.getReasonPhrase(), e.getMessage(), request.getRequestURI());
	}
}
