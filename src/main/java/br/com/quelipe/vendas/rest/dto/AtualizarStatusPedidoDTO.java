package br.com.quelipe.vendas.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtualizarStatusPedidoDTO {

	private String tpStatus;
}
