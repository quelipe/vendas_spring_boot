package br.com.quelipe.vendas.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.quelipe.vendas.domain.entity.Usuario;
import br.com.quelipe.vendas.exceptions.RegraNegocioException;
import br.com.quelipe.vendas.exceptions.DadosAutenticacaoInvalidoException;
import br.com.quelipe.vendas.jwt.JwtService;
import br.com.quelipe.vendas.rest.dto.CredenciaisDTO;
import br.com.quelipe.vendas.rest.dto.TokenDTO;
import br.com.quelipe.vendas.services.UsuarioService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/usuarios")
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private JwtService jwtService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario incluir(@RequestBody @Valid Usuario usuario) {
		String senha = encoder.encode(usuario.getDsSenha());
		usuario.setDsSenha(senha);
		return service.incluir(usuario);		
	}
	
	@PostMapping("/auth")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public TokenDTO login(@RequestBody @Valid CredenciaisDTO credenciais) {
		try {
			Usuario usuario = new Usuario();
			usuario.setDsSenha(credenciais.getDsSenha());
			usuario.setDsLogin(credenciais.getDsLogin());
			service.autenticar(usuario);
			
			String token = jwtService.gerarToken(usuario);
			return new TokenDTO(usuario.getDsLogin(), token);
			
		} catch(UsernameNotFoundException | DadosAutenticacaoInvalidoException e) {
			throw new RegraNegocioException(e.getMessage());
			
		}
	}
	
	
}
