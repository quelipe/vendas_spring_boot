package br.com.quelipe.vendas.rest.dto;

public class ItemPedidoDTO {

	private Integer produto;
	private Integer qtProduto;
	private Double vlUnitario;
	
	public Integer getProduto() {
		return produto;
	}
	public void setProduto(Integer produto) {
		this.produto = produto;
	}
	public Integer getQtProduto() {
		return qtProduto;
	}
	public void setQtProduto(Integer qtProduto) {
		this.qtProduto = qtProduto;
	}
	public Double getVlUnitario() {
		return vlUnitario;
	}
	public void setVlUnitario(Double vlUnitario) {
		this.vlUnitario = vlUnitario;
	}
	
	
}
