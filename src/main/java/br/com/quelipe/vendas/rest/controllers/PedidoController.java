package br.com.quelipe.vendas.rest.controllers;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.quelipe.vendas.domain.entity.Cliente;
import br.com.quelipe.vendas.domain.entity.ItemPedido;
import br.com.quelipe.vendas.domain.entity.Pedido;
import br.com.quelipe.vendas.domain.enums.StatusPedido;
import br.com.quelipe.vendas.exceptions.RecursoNaoEncontradoException;
import br.com.quelipe.vendas.rest.dto.AtualizarStatusPedidoDTO;
import br.com.quelipe.vendas.rest.dto.InformacaoItemPedidoDTO;
import br.com.quelipe.vendas.rest.dto.InformacoesPedidoDTO;
import br.com.quelipe.vendas.rest.dto.PedidoDTO;
import br.com.quelipe.vendas.services.ClienteService;
import br.com.quelipe.vendas.services.PedidoService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/pedidos")
public class PedidoController {

	@Autowired
	private PedidoService service;
	
	@Autowired
	private ClienteService cliService;
	
	@GetMapping
	public List<Pedido> getTodosPedidos() {
		return service.getTodosPedidos();
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluirPedido(@PathVariable Integer id) {
		service.excluirPedido(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Integer salvar(@RequestBody @Valid PedidoDTO dto) {
		Pedido pedido = service.salvar(dto);
		return pedido.getIdPedido();
	}
	
	@GetMapping(value = "/{id}")
	public InformacoesPedidoDTO findById(@PathVariable Integer id) {
		return service.obterPedidoCompleto(id)
				.map(p -> converter(p))
				.orElseThrow(() -> new RecursoNaoEncontradoException("Pedido não encontrado: "+id));
	}
	
	@GetMapping(value = "/{id}/cliente")
	public List<Pedido> findByIdCliente(@PathVariable Integer id) {
		Cliente cliente = cliService.findById(id);
		return service.getTodosPedidosCliente(cliente);

	}
	
	@PatchMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateStatus(@PathVariable Integer id, @RequestBody AtualizarStatusPedidoDTO dto) {
		String novoStatus = dto.getTpStatus();
		service.atualizarStatus(id, StatusPedido.valueOf(novoStatus));
	}
	
	private InformacoesPedidoDTO converter(Pedido pedido) {
		return InformacoesPedidoDTO
			.builder()
			.idPedido(pedido.getIdPedido())
			.dtPedido(pedido.getDtPedido().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
			.nrCpf(pedido.getCliente().getNrCpf())
			.nmCliente(pedido.getCliente().getNmCliente())
			.vlTotal(pedido.getTtPedido())
			.tpStatus(pedido.getTpStatus().name())
			.itens(convert(pedido.getItens()))
			.build();
			
	}
	
	private List<InformacaoItemPedidoDTO> convert(List<ItemPedido> itens) {
		if(CollectionUtils.isEmpty(itens)) {
			return Collections.emptyList();
		}
		return itens.stream().map(
				item -> InformacaoItemPedidoDTO
				.builder()
				.dsProduto(item.getProduto().getDsProduto())
				.vlUnitario(item.getProduto().getVlProduto())
				.qtProduto(item.getQtProduto())
				.build()
		).collect(Collectors.toList());
	}
}
