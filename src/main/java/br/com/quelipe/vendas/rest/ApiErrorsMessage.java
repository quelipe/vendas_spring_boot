package br.com.quelipe.vendas.rest;

import java.util.Arrays;
import java.util.List;

public class ApiErrorsMessage {

	private List<String> errors;
	
	public ApiErrorsMessage(String mensagemErro) {
		this.errors = Arrays.asList(mensagemErro);
	}
	
	public ApiErrorsMessage(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}
	
}
