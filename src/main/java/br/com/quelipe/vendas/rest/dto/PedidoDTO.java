package br.com.quelipe.vendas.rest.dto;

import java.util.List;

import br.com.quelipe.vendas.validations.NotEmptyList;
import jakarta.validation.constraints.NotNull;

public class PedidoDTO {
	
	@NotNull(message = "Informe o código do cliente.")
	private Integer cliente;
	
	@NotEmptyList(message = "Pedido não pode ser realizado sem itens.")
	private List<ItemPedidoDTO> itens;
	
	public Integer getCliente() {
		return cliente;
	}
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public List<ItemPedidoDTO> getItens() {
		return itens;
	}
	public void setItens(List<ItemPedidoDTO> itens) {
		this.itens = itens;
	}
	
	
}
