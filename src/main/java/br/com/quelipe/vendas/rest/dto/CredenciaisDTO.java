package br.com.quelipe.vendas.rest.dto;

import jakarta.validation.constraints.NotEmpty;

public class CredenciaisDTO {

	@NotEmpty(message = "Informe o Login")
	private String dsLogin;
	@NotEmpty(message = "Informe a senha")
	private String dsSenha;
	
	public CredenciaisDTO() {
		
	}
	
	public CredenciaisDTO(String dsLogin, String dsSenha) {
		this.dsLogin = dsLogin;
		this.dsSenha = dsSenha;
	}
	public String getDsLogin() {
		return dsLogin;
	}
	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}
	public String getDsSenha() {
		return dsSenha;
	}
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}
	
	
}
