package br.com.quelipe.vendas.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.quelipe.vendas.domain.entity.Cliente;
import br.com.quelipe.vendas.services.ClienteService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteService service;

	@GetMapping
	public List<Cliente> findAll() {
		return service.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public Cliente findById(@PathVariable Integer id) {
		return service.findById(id);
	}
	
	@GetMapping("/filtro")
	public List<Cliente> pesquisar(Cliente filtro) {
		return service.pesquisar(filtro);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente incluir(@RequestBody @Valid Cliente cliente) {
		return service.incluir(cliente);		
	}
	
	@PutMapping("{id}")
	public Cliente atualizar(@PathVariable Integer id, @RequestBody @Valid Cliente cliente) {
		return service.atualizar(id, cliente);
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		service.excluir(id);
	}
	
	
}
