package br.com.quelipe.vendas.rest.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InformacoesPedidoDTO {

	private Integer idPedido;
	private String nrCpf;
	private String nmCliente;
	private Double vlTotal;
	private String dtPedido;
	private String tpStatus;
	private List<InformacaoItemPedidoDTO> itens;
}
