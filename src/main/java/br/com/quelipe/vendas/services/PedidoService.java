package br.com.quelipe.vendas.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.quelipe.vendas.domain.entity.Cliente;
import br.com.quelipe.vendas.domain.entity.ItemPedido;
import br.com.quelipe.vendas.domain.entity.Pedido;
import br.com.quelipe.vendas.domain.entity.Produto;
import br.com.quelipe.vendas.domain.enums.StatusPedido;
import br.com.quelipe.vendas.domain.repositories.ClienteRepository;
import br.com.quelipe.vendas.domain.repositories.ItemPedidoRepository;
import br.com.quelipe.vendas.domain.repositories.PedidoRepository;
import br.com.quelipe.vendas.domain.repositories.ProdutoRepository;
import br.com.quelipe.vendas.exceptions.RecursoNaoEncontradoException;
import br.com.quelipe.vendas.exceptions.RegraNegocioException;
import br.com.quelipe.vendas.rest.dto.ItemPedidoDTO;
import br.com.quelipe.vendas.rest.dto.PedidoDTO;

@Service
public class PedidoService {
	
	@Autowired
	PedidoRepository repository;
	
	@Autowired
	ClienteRepository repoCliente;
	
	@Autowired
	ProdutoRepository repoProduto;
	
	@Autowired
	ItemPedidoRepository repoItemPedido;

	@Transactional
	public Pedido salvar(PedidoDTO dto) {
		Integer idCliente = dto.getCliente();
		Cliente cliente = repoCliente
				.findById(idCliente)
				.orElseThrow(() -> new RegraNegocioException("Código de cliente inválido"));
		
		Pedido pedido = new Pedido();
		pedido.setDtPedido(LocalDate.now());
		pedido.setCliente(cliente);
		pedido.setTpStatus(StatusPedido.REALIZADO);
		
		List<ItemPedido> itensPedido = converterItens(pedido, dto.getItens());
		repository.save(pedido);
		
		repoItemPedido.saveAll(itensPedido);
		pedido.setItens(itensPedido);
		
		return pedido;
		
	}
	
	private List<ItemPedido> converterItens(Pedido pedido, List<ItemPedidoDTO> itens) {
		if(itens.isEmpty()) {
			throw new RegraNegocioException("Não é possível realizar pedido sem itens");
		}
		return itens.stream()
			.map(dto -> {
			Integer idProduto = dto.getProduto();
			Produto produto = repoProduto
				.findById(idProduto)
				.orElseThrow(() -> new RegraNegocioException("Código do produto inválido: "+ idProduto));
			ItemPedido itemPedido = new ItemPedido();
			itemPedido.setQtProduto(dto.getQtProduto());
			itemPedido.setPedido(pedido);
			itemPedido.setVlUnitario(produto.getVlProduto());
			itemPedido.setProduto(produto);
			
			return itemPedido;
		}).toList();
	}
	
	public Optional<Pedido> obterPedidoCompleto(Integer id) {
		return repository.findByIdFetchItens(id);
	}
	
	@Transactional
	public void atualizarStatus(Integer id, StatusPedido status) {
		repository.findById(id)
			.map(pedido -> {
				pedido.setTpStatus(status);
				return repository.save(pedido);
			}).orElseThrow(() -> new RecursoNaoEncontradoException("Pedido não encontrado"));
	}
	
	public List<Pedido> getTodosPedidos() {
		return repository.findAll();
	}
	
	public List<Pedido> getTodosPedidosCliente(Cliente cliente) {
		return repository.findByCliente(cliente);
	}

	public void excluirPedido(Integer id) {
		repository.findById(id)
			.map( pedido -> {
				repository.delete(pedido);
				return Void.TYPE;
			}).orElseThrow(() -> new RecursoNaoEncontradoException("Pedido não encontrado"));
	}
}
