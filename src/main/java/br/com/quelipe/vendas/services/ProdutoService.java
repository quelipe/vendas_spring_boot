package br.com.quelipe.vendas.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;

import br.com.quelipe.vendas.domain.entity.Produto;
import br.com.quelipe.vendas.domain.repositories.ProdutoRepository;
import br.com.quelipe.vendas.exceptions.RecursoNaoEncontradoException;

@Service
public class ProdutoService {

	@Autowired
	ProdutoRepository repository;
	
	public List<Produto> findAll() {
		return repository.findAll();
	}
	
	public Produto findById(Integer id) {
		return repository
				.findById(id)
				.orElseThrow(() -> new RecursoNaoEncontradoException("Produto não encontrado"));
	}
	
	public List<Produto> pesquisar(Produto filtro) {
		ExampleMatcher match = ExampleMatcher
						.matching()
						.withIgnoreCase()
						.withStringMatcher(StringMatcher.CONTAINING);
		Example<Produto> example = Example.of(filtro, match);
		return repository.findAll(example);
	}
	
	public Produto incluir(Produto produto) {
		return repository.save(produto);
	}
	
	public Produto atualizar(Integer id, Produto produto) {
		return repository.findById(id)
		.map(prodExiste -> {
			produto.setIdProduto(prodExiste.getIdProduto());
			repository.save(produto);
			return produto;
		}).orElseThrow(() -> new RecursoNaoEncontradoException("Produto não encontrado"));
	}
	
	
	public void excluir(Integer id) {
		repository.findById(id)
		.map(prod -> {
			repository.delete(prod);
			return Void.TYPE;
		}).orElseThrow(() -> new RecursoNaoEncontradoException("Produto não encontrado"));

	}
}
