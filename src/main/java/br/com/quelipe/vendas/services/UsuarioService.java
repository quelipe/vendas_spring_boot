package br.com.quelipe.vendas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.quelipe.vendas.domain.entity.Usuario;
import br.com.quelipe.vendas.domain.repositories.UsuarioRepository;
import br.com.quelipe.vendas.exceptions.DadosAutenticacaoInvalidoException;

@Service
public class UsuarioService implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private PasswordEncoder encoder;
	
	public UserDetails autenticar(Usuario usuario) {
		UserDetails user = loadUserByUsername(usuario.getDsLogin());
		boolean ok = encoder.matches(usuario.getDsSenha(), user.getPassword());
		if(ok) {
			return user;
		}
		throw new DadosAutenticacaoInvalidoException();
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = repository.findByDsLogin(username).orElseThrow(() -> new DadosAutenticacaoInvalidoException());

		return User
				.builder()
				.username(usuario.getDsLogin())
				.password(usuario.getDsSenha())
				.roles("ADMIN")
				.build();
	}
	
	public Usuario incluir(Usuario usuario) {
		return repository.save(usuario);
	}

}
