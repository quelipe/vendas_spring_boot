package br.com.quelipe.vendas.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;

import br.com.quelipe.vendas.domain.entity.Cliente;
import br.com.quelipe.vendas.domain.repositories.ClienteRepository;
import br.com.quelipe.vendas.exceptions.RecursoNaoEncontradoException;

@Service
public class ClienteService {

	@Autowired
	ClienteRepository repository;
	
	public List<Cliente> findAll() {
		return repository.findAll();
	}
	
	public Cliente findById(Integer id) {
		return repository
				.findById(id)
				.orElseThrow(() -> new RecursoNaoEncontradoException("Cliente não encontrado: "+id));
	}
	
	public List<Cliente> pesquisar(Cliente filtro) {
		ExampleMatcher match = ExampleMatcher
						.matching()
						.withIgnoreCase()
						.withStringMatcher(StringMatcher.CONTAINING);
		Example<Cliente> example = Example.of(filtro, match);
		return repository.findAll(example);
	}
	
	public Cliente incluir(Cliente cliente) {
		return repository.save(cliente);
	}
	
	public Cliente atualizar(Integer id, Cliente cliente) {
		return repository.findById(id)
		.map(cliExiste -> {
			cliente.setIdCliente(cliExiste.getIdCliente());
			repository.save(cliente);
			return cliente;
		}).orElseThrow(() -> new RecursoNaoEncontradoException("Cliente não encontrado"));
	}
	
	
	public void excluir(Integer id) {
		repository.findById(id)
		.map(cli -> {
			repository.delete(cli);
			return Void.TYPE;
		}).orElseThrow(() -> new RecursoNaoEncontradoException("Cliente não encontrado"));
	}
	
}
