package br.com.quelipe.vendas.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.quelipe.vendas.domain.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

	//List<Cliente> findByNmClienteLike(String nmCliente);
	
}
