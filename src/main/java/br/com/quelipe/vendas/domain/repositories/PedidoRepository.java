package br.com.quelipe.vendas.domain.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.quelipe.vendas.domain.entity.Cliente;
import br.com.quelipe.vendas.domain.entity.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Integer> {
	
	List<Pedido> findByCliente(Cliente cliente);
	
	@Query(" select p from Pedido p left join fetch p.itens where p.idPedido = :id ")
	Optional<Pedido> findByIdFetchItens(Integer id);
	
	@Query(" select c from Cliente c left join fetch c.pedidos where c.idCliente = :id")
	List<Pedido> findClienteFetchPedidos(@Param("id") Integer id);
}
