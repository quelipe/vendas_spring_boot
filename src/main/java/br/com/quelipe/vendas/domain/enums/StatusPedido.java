package br.com.quelipe.vendas.domain.enums;

public enum StatusPedido {

	REALIZADO,
	CANCELADO;
}
