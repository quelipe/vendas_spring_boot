package br.com.quelipe.vendas.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.quelipe.vendas.domain.entity.ItemPedido;

public interface ItemPedidoRepository extends JpaRepository<ItemPedido, Integer> {
}
