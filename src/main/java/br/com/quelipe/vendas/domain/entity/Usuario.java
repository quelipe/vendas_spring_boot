package br.com.quelipe.vendas.domain.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;

@Entity
@Table(name = "tb_usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
	@NotEmpty(message = "O login é obrigatório")
	private String dsLogin;
	@NotEmpty(message = "A senha é obrigatória")
	private String dsSenha;
	
	public Usuario() {}
	
	public Usuario(Integer idUsuario, String dsLogin, String dsSenha) {
		super();
		this.idUsuario = idUsuario;
		this.dsLogin = dsLogin;
		this.dsSenha = dsSenha;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getDsLogin() {
		return dsLogin;
	}
	public void setDsLogin(String dsLogin) {
		this.dsLogin = dsLogin;
	}
	public String getDsSenha() {
		return dsSenha;
	}
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}
	
	
}
