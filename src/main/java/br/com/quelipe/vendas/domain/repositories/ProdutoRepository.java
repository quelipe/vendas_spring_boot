package br.com.quelipe.vendas.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.quelipe.vendas.domain.entity.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
}
