package br.com.quelipe.vendas.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.quelipe.vendas.domain.enums.StatusPedido;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "tb_pedido")
public class Pedido implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPedido;
	private LocalDate dtPedido;
	@Transient
	private Double vlPedido;
	
	@Enumerated(EnumType.STRING)
	private StatusPedido tpStatus;
	
	@ManyToOne
	@JoinColumn(name = "id_cliente")
	@JsonIgnore
	private Cliente cliente;
	
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<ItemPedido> itens; 
	
	public Integer getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public LocalDate getDtPedido() {
		return dtPedido;
	}
	public void setDtPedido(LocalDate dtPedido) {
		this.dtPedido = dtPedido;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}
	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}	
	public StatusPedido getTpStatus() {
		return tpStatus;
	}
	public void setTpStatus(StatusPedido tpStatus) {
		this.tpStatus = tpStatus;
	}
	public Double getTtPedido() {
		Double total = 0.0;
		for(ItemPedido obj: itens) {
			Double vlItem = obj.getVlUnitario();
			Integer qtd = obj.getQtProduto();
			total += vlItem * qtd;
		}
		return total;
	}
}
