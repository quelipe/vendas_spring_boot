INSERT INTO tb_cliente (nm_cliente, nr_cpf) VALUES ('João da silva', '79882900171');

INSERT INTO tb_produto (ds_produto, vl_produto) values ('Notebook I7', 3000.00);
INSERT INTO tb_produto (ds_produto, vl_produto) values ('Monitor 23 polegadas', 1500.00);
INSERT INTO tb_produto (ds_produto, vl_produto) values ('Teclado Gamer', 150.55);
INSERT INTO tb_produto (ds_produto, vl_produto) values ('Mouse Gamer', 95.00);

INSERT INTO tb_usuario (ds_login, ds_senha) values ('Adm', '$2a$10$FtaHRZxheuCskyv/cC3Zre6VVb3ywAyEmd.WkK58Ms5ZYwWSkk72K');